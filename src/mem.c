#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
  struct region allocated;
  const size_t needed_size = size_from_capacity( (block_capacity) {.bytes = query}).bytes;
  const block_size actual_size = (block_size) {.bytes = region_actual_size(needed_size)};
  void* mapping = map_pages(addr, actual_size.bytes, MAP_FIXED_NOREPLACE);
  if(mapping == MAP_FAILED) {
  	mapping = map_pages(addr, actual_size.bytes, 0);
  	if (mapping == MAP_FAILED)
  		return REGION_INVALID;
  	else {
  		block_init(mapping, actual_size, NULL);
  		allocated = (struct region) {.addr = mapping, .size = actual_size.bytes, .extends = false};
  	}
  	return allocated;
  }
  block_init(mapping, actual_size, NULL);
  allocated = (struct region) {.addr = mapping, .size = actual_size.bytes, .extends = true};
  return allocated;
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}


static bool split_if_too_big( struct block_header* block, size_t query ) {
  if(block_splittable(block, query)) {
  	// Creating the block that will serve as another half of our old block
  	struct block_header* add_block = (struct block_header*) ((uint8_t *) block + query + offsetof(struct block_header, contents));
  	block_size new_size = {.bytes = block->capacity.bytes - query};
  	// Initializing new block
  	block_init((void*) add_block, new_size, block->next);
  	// Connecting two blocks in a valid way
  	block->capacity.bytes = query;
  	block->next = add_block;
  	return true;
  } else return false;
  
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  struct block_header* next_header = block->next;
  if(block && next_header && mergeable(block, next_header)) {
  	block->capacity.bytes = block->capacity.bytes + size_from_capacity(next_header->capacity).bytes;
  	block->next = next_header->next;
  	return true;
  }
  return false;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
  struct block_search_result result = {0};
  //struct block_header* current = block;
  //struct block_header* found = NULL;
  if(!block) {
  	result.type = BSR_CORRUPTED;
  	result.block = NULL;
  	return result;
  }
  while(block) {
   	//while (!block_is_big_enough(sz, current) && current->is_free && merge_possible)
     	//	merge_possible = try_merge_with_next(current);
     	while (block->is_free && block->next && block->next->is_free)
     		if(!try_merge_with_next(block)) break;

  	if(block_is_big_enough(sz, block) && block->is_free) {
  		result.type = BSR_FOUND_GOOD_BLOCK;
  		result.block = block;
  		return result;
  	}
  	
  	if (block->next == NULL) { 
		result.type = BSR_REACHED_END_NOT_FOUND;
  		result.block = block;
  		return result;
	}

  	//found = current;
  	block = block->next;
  }
  return result;
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  if (query < BLOCK_MIN_CAPACITY) 
  	query = BLOCK_MIN_CAPACITY;
  struct block_search_result result = find_good_or_last (block, query);
  if (result.type == BSR_FOUND_GOOD_BLOCK) {
  	split_if_too_big(result.block, query);
    	result.block->is_free = false; 
  }
  return result;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  if (query < BLOCK_MIN_CAPACITY) 
  	query = BLOCK_MIN_CAPACITY;
  struct region new_region = alloc_region(block_after(last), query);
  if(!region_is_invalid(&new_region)) {
  	block_init(new_region.addr, (block_size) {.bytes = new_region.size}, NULL);
  	struct block_header* grown = new_region.addr;
  	last->next = grown;
  	if (last->is_free) {
  		if(try_merge_with_next(last))
  			return last;
  	}
  	return grown;
  }
  return NULL;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
  if (query < BLOCK_MIN_CAPACITY) 
  	query = BLOCK_MIN_CAPACITY;
  struct block_search_result alloc_result = try_memalloc_existing(query, heap_start);
  if(alloc_result.type == BSR_FOUND_GOOD_BLOCK) {
  	split_if_too_big(alloc_result.block, query);
  	alloc_result.block->is_free = false;
  } else if (alloc_result.type != BSR_CORRUPTED) {
  	grow_heap(alloc_result.block, query);
  	alloc_result = try_memalloc_existing(query, heap_start);
  	if(alloc_result.type == BSR_REACHED_END_NOT_FOUND)
  		return NULL;
  	else if (alloc_result.type != BSR_CORRUPTED) {
  		split_if_too_big(alloc_result.block, query);
  		alloc_result.block->is_free = false;
  	}
  }
  return alloc_result.block;
}


void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  // Merging all other blocks to free all requested allocated memory
  while(header && header->next && header->next->is_free) {
  	 bool keep_merging = try_merge_with_next(header);
	 if(!keep_merging)
	 	break;
  }
}
