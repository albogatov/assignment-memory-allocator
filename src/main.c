#define _DEFAULT_SOURCE
#include <unistd.h>
#include "mem.h"
#include "mem_internals.h"



struct block_header* block;

// Initializing test environment
static bool prepare_tests() {
  void* heap = heap_init(10000);
  block = (struct block_header*) heap;
  if(!heap || !block)
  	return false;
  return true;
}

// Counting the amount of allocated blocks
static int block_count(struct block_header* block) {
  int count = 0;
  struct block_header* iter_block = block;
  while(iter_block->next) {
  	if (!iter_block->is_free) count++;
  	iter_block = iter_block->next;
  }
  return count;
}


//Обычное успешное выделение памяти.
static void test1() {
  void* mem_test = _malloc(1000);
  if(!mem_test || block->capacity.bytes != 1000)
  	printf("Memory not allocated on test 1\n");
  else printf("Test 1 passed!\n");
  _free(mem_test);
}


//Освобождение одного блока из нескольких выделенных.
static void test2() {
  void* mem_test1 = _malloc(2000);
  void* mem_test2 = _malloc(3000);
  if(block_count(block) != 2) {
  	printf("Test 2 failed, incorrect number of blocks allocated\n");
  	_free(mem_test1);
  	_free(mem_test2);
  } else {
  	_free(mem_test1);
  	if(block_count(block) == 1)
  		printf("Test 2 passed!\n");
  	else printf("Memory freed incorrectly, test 2 failed, blocks: %d \n", block_count(block));
  	_free(mem_test2);
  }
}


//Освобождение двух блоков из нескольких выделенных.
static void test3() {
  void* mem_test1 = _malloc(2000);
  void* mem_test2 = _malloc(3000);
  void* mem_test3 = _malloc(4000);
  if(block_count(block) != 3) {
  	printf("Test 3 failed, incorrect number of blocks allocated\n");
  	_free(mem_test1);
  	_free(mem_test2);
  	_free(mem_test3);
  } else {
  	// Marking 2 blocks as free, as a result of merging we must remain with only one block
  	_free(mem_test1);
  	_free(mem_test2);
  	if(block_count(block) == 1)
  		printf("Test 3 passed!\n");
  	else printf("Memory freed incorrectly, test 3 failed, blocks: %d \n", block_count(block));
  	_free(mem_test3);
  }
}


//Память закончилась, новый регион памяти расширяет старый.
static void test4() {
  // Allocating enough memory to overfill the initial heap size
  void* mem_test1 = _malloc(9000);
  void* mem_test2 = _malloc(2000);
  // Checking if the heap was grown through the number of allocated blocks
  if(block_count(block) != 2)
  	printf("Test 4 failed, heap not grown\n");
  else printf("Test 4 passed!\n");
  _free(mem_test1);
  _free(mem_test2);
}
//Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте.
static void test5() {
    // Reaching the latest block of our old heap
    while(block->next)
    	block = block->next;
    void* addr1 = block + block->capacity.bytes;
    // Using logic similair to static mem.c function round_pages to calculate a proper allocation address for a new region
    mmap( (uint8_t*) (getpagesize() * ((size_t) addr1 / getpagesize() + 		(((size_t) addr1 % getpagesize()) > 0))),
                       1000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
    void* allocated = _malloc(10000);
    // Comparing starting address of the last block in an old heap to the first block in a new heap
    if((uint8_t*) block->next == ((uint8_t*) allocated - offsetof(struct block_header, contents))) {
        printf("Test 5 failed, memory was allocated next to the old heap\n");
    } else printf("Test 5 passed!\n");
    _free(allocated);
}

int main() {
  if(prepare_tests()) {
  	test1();
  	test2();
  	test3();
  	test4();
  	test5();
  	printf("Testing over\n");
  } else printf("Could not prepare testing environment\n");
}
